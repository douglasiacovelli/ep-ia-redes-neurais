/**
 * Parametros obrigatorios
 *
 * trainingData: Nome do arquivo do conjunto de dados de treino
 * validationData: Nome do arquivo do conjunto de dados de validação
 * testData: Nome do arquivo do conjunto de dados de teste
 * alpha: (0 < alpha <= 1) Taxa de aprendizagem
 * layerZ: Numero de neuronios na camada escondida
 * weights: (zero/aleatorio) Inicializao dos pesos
 * problemType: (binario/multiclasse) Se o problema possui saida binária ou multiclasse
 *
 * Parametros adicionais obrigatorios
 *
 * alphaDecreasing: (true/false) Decremento do alpha de 0.001 a cada epoca
 * shuffle: (true/false) Embaralhar os testes a cada epoca
 * layerY: (1/n) <N válido apenas para multiclasse> Problema multiclasse com 1 ou N neuronios na camada de saida, sendo N o numero de classes
 *
 * Execução do algoritmo
 *
 * java Main <trainingData> <validationData> <testData> <alpha> <layerZ> <weights> <problemType> <maxError> <alphaDecreasing> <shuffle> <layerY>
 *
 */


java Main data/xor.tra data/xor.val data/xor.tes 0.5 2 aleatorio binario false true 1
java Main data/optdigits.tra data/optdigits.val data/optdigits.tes 0.5 60 aleatorio multiclasse true true 1
java Main data/wdbc.tra data/wdbc.val data/wdbc.tes 0.5 30 aleatorio binario true true 1
java Main data/wdbc.tra data/wdbc.val data/wdbc.tes 0.5 30 zero binario false false 1
