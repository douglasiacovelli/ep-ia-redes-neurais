public class DataStateMLP{

	//Classe que escreve atributos (pesos XZ, ZY, biasZ, biasY, epoca e error) em arquivo a cada 10 épocas.
	static DataStateMLP[] dataStates;

	double[][] pesosXZ;
	double[][] pesosZY; 
	double[] biasZ;
	double[] biasY;
	int epoch;
	double error;
	String confusionMatrix;


	public DataStateMLP(){
		this.epoch = -1;
		this.error = 999; //erro alto para que seja sobrescrito em saveDataStateMLP
	}

	public DataStateMLP(double[][] pesosXZ, double[][] pesosZY, double[] biasZ, double[] biasY, int epoch, double error, String confusionMatrix){
		this.pesosXZ = pesosXZ;
		this.pesosZY = pesosZY;
		this.biasZ = biasZ;
		this.biasY = biasY;
		this.epoch = epoch;
		this.error = error;
		this.confusionMatrix = confusionMatrix;
	}

	public static void initialize(){
		dataStates = new DataStateMLP[10];

		for (int dataState = 0; dataState < dataStates.length; dataState++) {
			dataStates[dataState] = new DataStateMLP();
		}
	}

	public static void saveDataStateMLP(double[][] pesosXZ, double[][] pesosZY, double[] biasZ, double[] biasY, int epoch, double error, String confusionMatrix){
		
		double maxError = 0;
		int maxErrorIndex = -1;
		int dataState;

		for (dataState = 0; dataState < dataStates.length; dataState++) {
			//Se o objeto atual tiver um erro maior que o maior erro, então o maior erro é substituido e o index do maior error vira o atual
			if(dataStates[dataState].error > maxError){
				maxError = dataStates[dataState].error;
				maxErrorIndex = dataState;
			}
		}




		if(maxError > error){
		
						
			double[][] originalPesosXZ = new double[pesosXZ.length][pesosXZ[0].length];
			double[][] originalPesosZY = new double[pesosZY.length][pesosZY[0].length]; 
			double[] originalBiasZ = new double[biasZ.length];
			double[] originalBiasY = new double[biasY.length];

			for(int z = 0; z < pesosZY.length; z++){
				for(int y = 0; y < pesosZY[0].length; y++){
					originalPesosZY[z][y] = pesosZY[z][y];
				}
				
			}
			
			for(int y = 0; y < biasY.length; y++){
				originalBiasY[y] = biasY[y];
			}
		
			for(int x = 0; x < pesosXZ.length; x++){
				for(int z = 0; z < pesosXZ[0].length; z++){
					originalPesosXZ[x][z] = pesosXZ[x][z];
				}
			}
			
			for(int z = 0; z < biasZ.length; z++){
				originalBiasZ[z] = biasZ[z];
			}
			

			dataStates[maxErrorIndex] = new DataStateMLP(originalPesosXZ, originalPesosZY, originalBiasZ, originalBiasY, epoch, error, confusionMatrix);
		}
	}

	static public void writeDataStateMLP(){
		String data = "";
		int index = 0;
		for (DataStateMLP dataState : dataStates) {
			if(dataState.pesosXZ != null){

				data += index+"\n";
				data += "Epoch: "+dataState.epoch+"\n";
				data += "Error: "+String.format("%.18f",dataState.error)+"\n";
				
				boolean printWeights = false;

				if(printWeights){
					
					data += "\n";
					data += "Pesos ZY: ";
					for(int z = 0; z < dataState.pesosZY.length; z++){
						for(int y = 0; y < dataState.pesosZY[0].length; y++){
							data += dataState.pesosZY[z][y]+" ";
						}
						
					}
					data += "\n";
					for(int y = 0; y < dataState.biasY.length; y++){
						data += "Bias Y: "+dataState.biasY[y]+"\n";
					}

					data += "\n";
					data += "Pesos XZ: ";			
					for(int x = 0; x < dataState.pesosXZ.length; x++){
						for(int z = 0; z < dataState.pesosXZ[0].length; z++){
							data += dataState.pesosXZ[x][z]+" ";
						}
					}
					data += "\n";
					for(int z = 0; z < dataState.biasZ.length; z++){
						data += "Bias Z: "+dataState.biasZ[z]+"\n";
					}
				}
				data += "\n\nConfusion Matrix\n";
				data += dataState.confusionMatrix;
				data += "-------------------------------\n\n";

				index++;
			}
		}
		DataManagement.writeData("dataStateMLP", data);
	}
	
	public static DataStateMLP findMinimumError(){
		int minIndex = 0;
		for(int i = 1; i< dataStates.length; i++){
		
			if(dataStates[minIndex].error > dataStates[i].error){
				
				minIndex = i;	
			}
		}
		System.out.println("Best Epoch: "+dataStates[minIndex].epoch);
		return dataStates[minIndex];
	}
}