/*
	Normalização implementada com base em: http://www.acit2k.org/ACIT/2012Proceedings/13233.pdf
*/

import java.util.*;

public class PreProcessing{
	
	public static ArrayList<ArrayList<Double>> normalize(ArrayList<ArrayList<Double>> data){ //DataSet
		

		/**
		 * Find MAX and MIN values
		 */

		for (int atributo = 0; atributo < data.get(0).size(); atributo++) {
			double MAX = -9999999.0;
			double MIN = 999999.0;

			for (int linha = 0; linha < data.size(); linha++) {
				
				double value = data.get(linha).get(atributo);
				
				if(value > MAX){
					MAX = value;
				}

				if(value < MIN){
					MIN = value;
				}
				
			}
			
			/**
			 * Normalizar os valores para este atributo.
			 * 
			 * A normalização é feita neste for, pois o MAX e o MIN são relativos a cada atributo.
			 */
			
			for (int linha = 0; linha < data.size(); linha++) {
				
				double actualValue = data.get(linha).get(atributo);

				if(MAX - MIN == 0){
					continue;
				}
				double newValue = (actualValue - MIN)/(MAX-MIN);
				
				data.get(linha).set(atributo, newValue);

			}
		}
		return data;
	}

	public static double[] normalize(double[] data){ //Normalização do Target
		

		/**
		 * Find MAX and MIN values
		 */

		double MAX = -9999999.0;
		double MIN = 999999.0;

		for (int linha = 0; linha < data.length; linha++) {
			
			double value = data[linha];
			if(value > MAX){
				MAX = value;
			}

			if(value < MIN){
				MIN = value;
			}
			
		}
		
		/**
		 * Normalizar os valores para este atributo.
		 * 
		 * A normalização é feita neste for, pois o MAX e o MIN são relativos a cada atributo.
		 */
			
		for (int linha = 0; linha < data.length; linha++) {
			
			double actualValue = data[linha];

			if(MAX - MIN == 0){
				continue; //Pula este 
			}
			double newValue = (actualValue - MIN)/(MAX-MIN);
			
			data[linha] = newValue;

		}
		
		return data;
	}
}