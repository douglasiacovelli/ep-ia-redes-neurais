import java.io.*;
import java.util.*;
import java.text.SimpleDateFormat;

public class DataManagement{

	static FileReader reader;
	static BufferedReader leitor;
	static ArrayList<String> originalDataSet;

	static Data readData(String nomeEntrada, String layerY){
		Data data = new Data();

		String[] atributos = null;
		String linhaAtual = "";
		String linha = null;
		originalDataSet = new ArrayList<String>();

		// carregar todas as palavras em cada elemento da estrutura(Lista ligada ou arranjo)
		
		
		try{
			reader = new FileReader(nomeEntrada); 
			leitor = new BufferedReader(reader);

			while ((linha = leitor.readLine()) != null) {
				originalDataSet.add(linha); //adiciona as linhas do arquivo dentro da estrutura de dados dataSet sem formatação
			}

			leitor.close();

		}catch(Exception e){

			System.out.println(e.getMessage());
			System.err.println("Erro na leitura");
		}

		data.dataSet = new ArrayList<ArrayList<Double>>(originalDataSet.size());

		data.targets = new ArrayList<ArrayList<Double>>(originalDataSet.size());

		

		/*
		 *
		 * FOR para adicionar os dataSets na estrutura de dados e contar o número de valores de targets diferentes
		 *
		 */

		for(int linhaDoArquivo = 0; linhaDoArquivo < originalDataSet.size(); linhaDoArquivo++){
			
			data.dataSet.add(linhaDoArquivo, new ArrayList<Double>()); //Criar arraylists dentro de cada linha (armazenará atributos)
			
			/**
			 * Preparar estrutura dos dataSets
			 */

			linhaAtual = originalDataSet.get(linhaDoArquivo); //pega a linha sem formatação

			atributos = linhaAtual.split("\\s*,\\s*"); //Separa atributos pela virgula e retira os espacos (trim)

			for(int atributo = 0; atributo < atributos.length-1; atributo++){

				data.dataSet.get(linhaDoArquivo).add(Double.parseDouble(atributos[atributo])); //Adiciona valores dos atributos no array dataSet
			}
			
			linhaAtual = null;
		}
		
		
			
		/*
		 * For para adicionar os valores dos targets ao data.targets tanto para multiclasse qnt binario
		 *
		 */
		ArrayList<Double> differentTargetsValues = null;

		if(MLP.problemType.equals("multiclasse")){
			differentTargetsValues = getDifferentTargetsValues();
		}

		for(int linhaDoArquivo = 0; linhaDoArquivo < originalDataSet.size(); linhaDoArquivo++){
			
			int valorASerAdicionado = 0;

			linhaAtual = originalDataSet.get(linhaDoArquivo); //pega a linha sem formatação
			atributos = linhaAtual.split("\\s*,\\s*");

			if(MLP.problemType.equals("multiclasse") && layerY.equals("n")){

				data.targets.add(linhaDoArquivo, new ArrayList<Double>(differentTargetsValues.size())); //Cria arraylist com n posições para cada target

				for (int j = 0; j < differentTargetsValues.size(); j++ ) {
					data.targets.get(linhaDoArquivo).add(j, 0.0);
				}

				valorASerAdicionado = Integer.parseInt(atributos[atributos.length-1]);//ex: valor é 3, ele adicionará na terceira posição do array o valor 1 e as outras serão 0

				data.targets.get(linhaDoArquivo).set(valorASerAdicionado, 1.0);
			
			}else{
				data.targets.add(linhaDoArquivo, new ArrayList<Double>());
				
				data.targets.get(linhaDoArquivo).add(Double.parseDouble(atributos[atributos.length-1]));	
			}
		}

		return data;
	}

	static ArrayList<Double> getDifferentTargetsValues(){
		ArrayList<Double> differentTargetsValues = new ArrayList<Double>();

		for(int linhaDoArquivo = 0; linhaDoArquivo < originalDataSet.size(); linhaDoArquivo++){
			
			String linhaAtual = originalDataSet.get(linhaDoArquivo);
			String[] atributos = linhaAtual.split("\\s*,\\s*");
			

			double valorDouble = Double.parseDouble(atributos[atributos.length-1]);//Converter string em double

			if(!differentTargetsValues.contains(valorDouble)){
				differentTargetsValues.add(valorDouble);
			}

		}

		return differentTargetsValues;
	}


	static void writeData(String fileName, String text){
		Date data = new Date();
		SimpleDateFormat formatData = new SimpleDateFormat("dd-MM-yyyy--hh-mm-ss");
		String date = formatData.format(data);
		text = Main.parameters +"\n"+ text;


		File dir = new File(fileName);
		if(!dir.exists()){
			dir.mkdir();
		}
		File f = new File(fileName+File.separator+date+"-"+fileName+".txt");

		try {
			FileWriter fw = new FileWriter(f); 
			PrintWriter pw = new PrintWriter(fw);
			pw.println(text);
			pw.close();
		} catch (IOException e){
			e.printStackTrace(); 
		}
	}

}