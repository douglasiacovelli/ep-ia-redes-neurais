public class ConfusionMatrix{
	static int TP = 0;// TRUE POSITIVE
	static int FP = 0; // FALSE POSITIVE
	static int FN = 0; // FALSE NEGATIVE
	static int TN = 0; // TRUE NEGATIVE
	static double TPR; // Sensibilidade : porcentagem de verdadeiros positivos dentre todos os exemplos cuja classe real eh positiva	
	static double FPR; // Taxa de Falso Positivo : porcentagem de exemplo cuja classe real eh negativa que sao classificados como positivos
	static double SPC; // Especificidade : proporcao de rejeicoes corretas entre os exemplos cuja classe real eh negativa
	static double PPV; // Precisao : proporcao de acertos dentre todos os exemplos preditos como positivos
	static double NPV; // Preditividade Negativa : proporcao de rejeicoes corretas dentre os exemplos preditos como negativos
	static double FDR; // Taxa de falsas descobertas : denota o numero de falsos positivos dentre os exemplos classificados como positivos
	static double acuracia;
	static double erro;
	static double fScore;

	static final double Beta = 1.0;


	static int[][] multiclasseMatrix;

	public static void resetVariables(){

		TP = 0;
		FP = 0;
		FN = 0;
		TN = 0;
		TPR = 0;
		FPR = 0;
		SPC = 0;
		PPV = 0;
		NPV = 0;
		FDR = 0;
		acuracia = 0;
		erro = 0;
		fScore = 0;
		
		if(MLP.problemType.equals("multiclasse")){
			multiclasseMatrix = new int[MLP.differentTargetsValues.size()][MLP.differentTargetsValues.size()];
		}
		
	}

	public static void add(double target, double guess){

		if(MLP.problemType.equals("multiclasse")){
			multiclasseMatrix[(int)target][(int)guess]++;
		}else{
			if(target == 1 && guess == 1){
				TP++;
			}
			if(target == 0 && guess == 1){
				FP++;
			}
			if(target == 1 && guess == 0){
				FN++;
			}
			if(target == 0 && guess == 0){
				TN++;
			}
		}


	}



	public static String print(){
		String print = "";
		int cont = 0;
		int acertos = 0;
		/*
		 * Exibe a matriz de confusao para multiclasses no terminal
		 */
		if(MLP.problemType.equals("multiclasse")){

			for(int i = 0; i < multiclasseMatrix.length; i++){
				for(int j = 0; j < multiclasseMatrix[0].length; j++){
					if(i == j) acertos += multiclasseMatrix[i][j];
					cont += multiclasseMatrix[i][j];
					print += multiclasseMatrix[i][j]+"\t";
					System.out.print(multiclasseMatrix[i][j]+"\t");
				}
				print += "\n";
				System.out.println();
			}
			System.out.println("% de acertos "+String.format("%.2f",(((double)acertos/(double)cont)*100)));

		/*
		 * Exibe a matriz de confusao para binarios no terminal
		 */

		}else{
			if(TP+FN != 0){
				TPR = (double)TP/(TP+FN) * 100; // Sensibilidade : porcentagem de verdadeiros positivos dentre todos os exemplos cuja classe real eh positiva	
			}
			if(TN+FP != 0){
				FPR = (double)FP/(TN+FP) * 100; // Taxa de Falso Positivo : porcentagem de exemplo cuja classe real eh negativa que sao classificados como positivos
			}
			if((FP+TN) != 0){
				SPC = (double)TN/(FP+TN) * 100; // Especificidade : proporcao de rejeicoes corretas entre os exemplos cuja classe real eh negativa
			}
			if((TP+FP) != 0){
				PPV = (double)TP/(TP+FP) * 100; // Precisao : proporcao de acertos dentre todos os exemplos preditos como positivos
			}
			if(TN+FN != 0){
				NPV = (double)TN/(TN+FN) * 100; // Preditividade Negativa : proporcao de rejeicoes corretas dentre os exemplos preditos como negativos		
			}
			if(TP+FP != 0){
				FDR = (double)FP/(TP+FP) * 100; // Taxa de falsas descobertas : denota o numero de falsos positivos dentre os exemplos classificados como positivos	
			}
			acuracia = (double)(TP+TN)/(TP+TN+FP+FN) * 100;
			erro = (double)(FP+FN)/(TP+TN+FP+FN) * 100;
			if(PPV==0||TPR==0) fScore = 0;
			//else fScore = 2/(1/(PPV/100)+(1/(TPR/100)));
			else fScore = (1+Math.pow(Beta,2)*(PPV/100)*(TPR/100))/(Math.pow(Beta,2)*(PPV/100)+(TPR/100));

			System.out.println("        -------------");
			System.out.println("        |  Predita  |");
			System.out.println("        |-----------|");
			System.out.println("        |  1  |  0  |");
			System.out.println("--------|-----|-----|");
			System.out.println("| R | 1 | "+String.format("%03d", TP)+" | "+String.format("%03d", FN)+" |");
			System.out.println("| e |___|_____|_____|");
			System.out.println("| a |   |     |     |");
			System.out.println("| l | 0 | "+String.format("%03d", FP)+" | "+String.format("%03d", TN)+" |");
			System.out.println("---------------------");
			System.out.println();
			System.out.println("Sensibilidade: "+String.format("%.2f", TPR)+"%");
			System.out.println("Taxa de Falso Positivos: "+String.format("%.2f", FPR)+"%");
			System.out.println("Especificidade: "+String.format("%.2f", SPC)+"%");
			System.out.println("Precisao: "+String.format("%.2f", PPV)+"%");
			System.out.println("Preditividade Negativa: "+String.format("%.2f", NPV)+"%");
			System.out.println("Taxa de falsas descobertas: "+String.format("%.2f", FDR)+"%");
			System.out.println("Acuracia: "+acuracia+"%");
			System.out.println("Erro: "+erro+"%");
			System.out.println("F-Score: "+fScore);
			System.out.println("ROC Y: "+TPR);
			System.out.println("ROC X: "+FPR);

			print += TP+"\t"+FN+"\n";
			print += FP+"\t"+TN+"\n";
			print += "\n\n";
			print += "Sensibilidade: "+String.format("%.2f", TPR)+"%\n";
			print += "Taxa de Falso Positivos: "+String.format("%.2f", FPR)+"%\n";
			print += "Especificidade: "+String.format("%.2f", SPC)+"%\n";
			print += "Precisao: "+String.format("%.2f", PPV)+"%\n";
			print += "Preditividade Negativa: "+String.format("%.2f", NPV)+"%\n";
			print += "Taxa de falsas descobertas: "+String.format("%.2f", FDR)+"%\n";
			print += "Acuracia: "+acuracia+"%\n";
			print += "Erro: "+erro+"%\n";
			print += "Curva ROC Eixo Y: "+TPR+"\n";
			print += "Curva ROC Eixo X: "+FPR+"%\n";
		}

		resetVariables();
		return print;
	}
}


