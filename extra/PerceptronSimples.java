import java.util.*;

public class PerceptronSimples{
	
	public static void main(String[] args){
		
		/**
		 * Dados correspondem ao problema AND
		 */

		int[][] input = {{1,0},
						 {0,1},
						 {0,0},
						 {1,1}};
		int[] targets = {-1,-1,-1,1};

		double[] weight = new double[input[0].length];
		
		//Para o Bias foi utilizado um array, para que ele fosse passado por referência no método
		double[] bias = {0};

		treinarRede(input, targets, weight, bias);

		System.out.println("\n\nPeso 1\tPeso 2\tBias");
		System.out.println(weight[0]+"\t"+weight[1]+"\t"+bias[0]+"\n");
	}

	public static void treinarRede(int[][] input, int[] targets, double[] weight, double[] bias){
		
		double start = System.currentTimeMillis();
		int epocas = 0;
		double learningRate = 0.5;
		double threshold = 0.2;

		/**
		 * ShuffleAux servirá para que sejam testados os dados de forma aleatória
		 */
		int[] shuffleAux = new int[input.length];
		for (int i = 0; i< input.length; i++ ) {
			shuffleAux[i] = i;
		}

		boolean weightChanged = true;
		/**
		 * Passo 1
		 */
		while(weightChanged){
			
			/**
			 * Passo 2 - Para cada par de treinamento
			 */
			
			epocas++;
			weightChanged = false;
			shuffleArray(shuffleAux);

			System.out.println("\nIn1:\tIn2:\tOut:");

			for (int i : shuffleAux) {
				
				/**
				 * Passo 3 - Instanciar os valores	
				 */
				
				int in[] = new int[input[0].length];

				for (int j = 0; j < input[0].length; j++) {
					in[j] = input[i][j];
				}
				int target = targets[i];

				/**
				 * Passo 4 - Computar as respostas das unidades de saída
				 */
				
				double y_in = bias[0];
				for (int j = 0; j<input[0].length; j++) {
					
					y_in += in[j]*weight[j];
				}

				int y = yIn(y_in, threshold);

				/**
				 * Passo 5 - Alterar os pesos e bias se ocorreu algum erro
				 */
				
				System.out.print(in[0]+"\t"+in[1]+"\t"+y);
				
				if(y != target){
					for (int j = 0; j < in.length; j++) {
						weight[j] = weight[j] + learningRate * target * in[j];
					}

					//É corrigido o peso do bias
					bias[0] = bias[0] + learningRate * target;

					weightChanged = true;

					System.out.println(" >> Valor Corrigido");
				}else{
					System.out.println();
				}
			}
		}
		double end = System.currentTimeMillis();
		double executionTime = end-start;

		System.out.println("\n\n EPOCAS: "+epocas);
		System.out.println("Executado em: "+executionTime+"ms");
	}

	public static int yIn(double y_in, double threshold){
		if(y_in > threshold){
			return 1;
		}else{
			if(y_in < -threshold){
				return -1;
			}else{
				return 0;
			}
		}
	}

	static void shuffleArray(int[] ar){
		Random rnd = new Random();
		for (int i = ar.length - 1; i >= 0; i--){
			int index = rnd.nextInt(i + 1);
			// Simple swap
			int a = ar[index];
			ar[index] = ar[i];
			ar[i] = a;
		}
	}
}