/*
	Algoritmo Multilayer Perceptron feito com base no livro "Fundamentals Of Neural Networks" pela Laurene Fausett

	Algoritmo implementado por:
		
		Alberto Kamiya
		Douglas Iacovelli
		Marcos Tadeu Silva
		Luiz Henrique Baptistão
*/

import java.util.*;
import java.lang.*;

public class MLP{
	
	static boolean debug = false;

	static double[][] pesosXZ;
	static double[][] pesosZY; 
	static double[] biasZ;
	static double[] biasY;

	static double[] xNeurons;
	static double[] zNeurons;
	static double[] yNeurons;

	static ArrayList<ArrayList<Double>> dataSet;
	static ArrayList<ArrayList<Double>> targets;

	static ArrayList<ArrayList<Double>> validationSet;
	static ArrayList<ArrayList<Double>> validationTargets;

	static ArrayList<ArrayList<Double>> testSet;
	static ArrayList<ArrayList<Double>> testTargets;

	static double[] zIn;
	static double[] yIn;

	static double[] deltinha;
	static double[] deltinhaInZ;

	static double[][] deltaZY;
	static double[] deltaBiasY;

	static double[][] deltaXZ;
	static double[] deltaBiasZ;

	static int actualEpoch = 0;

	static int MAXEPOCH = 30000;
	static double MAXERROR = 0.0000009;
	static double diff = 1;
	static double lastError = 0;
	static double actualError = 0;
	static String problemType;
	static String inicializeWeights;
	static int layerZ;
	static int layerY;
	static double alpha;
	static String writeData = "";
	static boolean alphaDecreasing;

	static double error = 10;
	
	static ArrayList<Double> differentTargetsValues;

	static boolean shuffle;

	static String targetSaida = "";


	private static void feedForward(int testIndex,ArrayList<ArrayList<Double>> data){
		zIn = new double[zNeurons.length];
		yIn = new double[yNeurons.length];
		/**
		 * Laco responsavel por instanciar os neuronios de entrada (X)
		 * com os valores de treinamento (testCase).
		 */
		for(int x = 0; x < xNeurons.length; x++) {

			xNeurons[x] = data.get(testIndex).get(x);

		}
		/**
		 * Percorre os neuronios de entrada, X (x1,...,xn),
		 * multiplicando o valor de x pelo peso e armazenando
		 * este valor na entrada dos neuronios Z (camada oculta).
		 *
		 * Soma o valor do BIAS Z.
		 *
		 * Por fim eh inserido em cada neuronio da camada Z
		 * o valor gerado pela funcao de ativacao usando como parametro
		 * a soma das entradas ja pesadas.
		 */
		
		for(int z = 0; z < zNeurons.length; z++){

			for(int x = 0; x < xNeurons.length; x++){
				zIn[z] += xNeurons[x] * pesosXZ[x][z];		

			}
			zIn[z] += biasZ[z];

			zNeurons[z] = activate_Function(zIn[z]);
		}

		/**
		 * Percorre os neuronios da camada oculta, z (z1,z2,...,zn),
		 * multiplicando o valor de z pelo peso e armazenando este
		 * valor na entrada dos neuronios Y (camada de saida).
		 *
		 * Soma o valor do BIAS Y.
		 *
		 * Por fim eh inserido em cada neuronio da camada Y
		 * o valor gerado pela funcao de ativacao usando como parametro
		 * a soma das entradas ja pesadas.
		 */

		for(int y = 0; y < yNeurons.length; y++){

			for(int z = 0; z < zNeurons.length; z++){

				yIn[y] += zNeurons[z] * pesosZY[z][y];

			}

			yIn[y] += biasY[y];

			yNeurons[y] = activate_Function(yIn[y]);
		}

		
	}


	static void backPropagation(int testIndex){

		deltinha = new double[yNeurons.length];
		deltinhaInZ = new double[zNeurons.length];
		deltaZY = new double[zNeurons.length][yNeurons.length];
		deltaXZ = new double[xNeurons.length][zNeurons.length];
		deltaBiasY = new double[yNeurons.length];
		deltaBiasZ = new double[zNeurons.length];

		// passo 6
		for(int y = 0; y < yNeurons.length; y++){
			
			deltinha[y] = (targets.get(testIndex).get(y) - yNeurons[y]) * derivate(yIn[y]);
			
			for(int z = 0; z < zNeurons.length; z++){
				
				deltaZY[z][y] = alpha * deltinha[y] * zNeurons[z];
			}

			deltaBiasY[y] = alpha * deltinha[y];
		}

		// passo7
		for (int z = 0; z < zNeurons.length; z++){

			for(int y = 0; y < yNeurons.length; y++){
			
				deltinhaInZ[z] += deltinha[y] * pesosZY[z][y];
			}
		}
		
		deltinha = new double[zNeurons.length];

		for (int z = 0; z < zNeurons.length; z++){
			deltinha[z] = deltinhaInZ[z] * derivate(zIn[z]);
		}


		for(int x = 0; x < xNeurons.length; x++){

			for(int z = 0; z < zNeurons.length; z++){

				deltaXZ[x][z] = alpha * deltinha[z] * xNeurons[x];
				deltaBiasZ[z] = alpha * deltinha[z];
			}
		}
	}

	static void updateWeights(){
		// passo 8
		for(int z = 0; z < zNeurons.length; z++){
			for(int y = 0; y < yNeurons.length; y++){
				pesosZY[z][y] = pesosZY[z][y] + deltaZY[z][y];

				biasZ[z] = biasZ[z] + deltaBiasZ[z];
			}
		}

		for(int y = 0; y < yNeurons.length; y++){
			biasY[y] = biasY[y] + deltaBiasY[y];
		}


		for(int x = 0; x < xNeurons.length; x++){
			for(int z = 0; z < zNeurons.length; z++){
				pesosXZ[x][z] = pesosXZ[x][z] + deltaXZ[x][z];
			}
		}

		for(int z = 0; z < zNeurons.length; z++){
			biasZ[z] = biasZ[z] + deltaBiasZ[z];
		}

	}

	/**
	 * Metodo responsavel por aplicar a funcao de ativacao
	 */

	private static double activate_Function(double z){
		double result = 1/(1 + Math.exp(-z));
		return result;	
	}

	/*
	 * Metodo responsavel por calcular a derivada
	 */

	private static double derivate(double z){
		double fz = activate_Function(z);
		double result = fz*(1 - fz);
		return result;
	}


	/*
	 * Embaralha os testes para evitar a inconvergencia
	 */

	static void shuffleArray(int[] ar){
		Random rnd = new Random();
		for (int i = ar.length - 1; i >= 0; i--){
			int index = rnd.nextInt(i + 1);
			// Simple swap
			int a = ar[index];
			ar[index] = ar[i];
			ar[i] = a;
		}
	}

	/*
	**************************************************
	*				 RUN PRACTICE SET				 *
	**************************************************
	*/

	public static void practice(){

		
		//Inicialização DataStateMLP
		DataStateMLP.initialize();

		
		

		actualEpoch = 0;
		/**
		 * Inicializar os neuronios das camadas X(entrada), Z(oculta) e Y(saida)
		 */

		xNeurons = new double[dataSet.get(0).size()];
		zNeurons = new double[layerZ];
		yNeurons = new double[layerY];

		
		/**
		 * Inicializar os pesos
		 *
		 * BIAS afeta todos os neuronios de entrada da camada seguinte
		 */

		pesosXZ = new double[xNeurons.length][zNeurons.length];
		pesosZY = new double[zNeurons.length][yNeurons.length];
		biasZ = new double[zNeurons.length];
		biasY = new double[yNeurons.length];

		if(inicializeWeights.equals("aleatorio")){
			for(int x = 0; x < xNeurons.length; x++){
				for(int z = 0; z < zNeurons.length; z++){
					pesosXZ[x][z] = Math.random() - 0.5;
				}
			}


			for(int z = 0; z < zNeurons.length; z++){
				for(int y = 0; y < yNeurons.length; y++){
					pesosZY[z][y] = Math.random() - 0.5;
				}

				biasZ[z] = Math.random() - 0.5;
			}

			for(int y = 0; y < yNeurons.length; y++){
				biasY[y] = Math.random() - 0.5;
			}
		}
		/**
		 * Preparacao do array que sera utilizado para que os testes sejam
		 * pegos de maneira aleatoria
		 */
		int[] shuffleAux = new int[dataSet.size()];
		for (int i = 0; i < shuffleAux.length; i++ ) {
			shuffleAux[i] = i;
		}

		/**
		 * Passo 2 (Laco de Epocas)
		 */


		while(diff > MAXERROR && actualEpoch < MAXEPOCH){

			actualEpoch++;
			if(shuffle) shuffleArray(shuffleAux);

			error = 0;

			for (int test : shuffleAux) {
				feedForward(test,dataSet);
				backPropagation(test);
				updateWeights();

				for(int y = 0; y < yNeurons.length; y++){
					error += Math.pow(targets.get(test).get(y) - yNeurons[y],2);
				}
				
			}
			

			error = error/(dataSet.size() * yNeurons.length); //test.size() é o denominador para que seja feita a média.




			if(actualEpoch % 10 == 0 || actualEpoch < 10){

				lastError = actualError;
				actualError = error;
				diff = Math.abs(actualError - lastError);

				System.out.println("diff: "+String.format("%.18f",diff) + " actual: " + actualError + " last:  " + lastError);
				writeData += actualEpoch+"\t"+String.format("%.18f",error)+"\t";
				targetSaida += "Epoch: "+actualEpoch+" \tError Training: "+String.format("%.18f", error);
				System.out.println("\nEpoch: "+actualEpoch);
				System.out.println("Error Training: \t"+error);
				validation();

				if(alphaDecreasing){
					if(alpha > 0.03){
					
						alpha = alpha-0.001;	
					}
					System.out.println("Alpha decrementado");	
				}
			}
			
		}
		DataManagement.writeData("result", writeData);
		updateBestWeight();

	}

	/*
	**************************************************
	*				 RUN VALIDATION SET				 *
	**************************************************
	*/

	static void validation(){
		String data = "\n";				
		data += "Pesos ZY: ";
		for(int z = 0; z < pesosZY.length; z++){
			for(int y = 0; y < pesosZY[0].length; y++){
				data += pesosZY[z][y]+" ";
			}
			
		}
		data += "\n";
		for(int y = 0; y < biasY.length; y++){
			data += "Bias Y: "+biasY[y]+"\n";
		}

		data += "\n";
		data += "Pesos XZ: ";			
		for(int x = 0; x < pesosXZ.length; x++){
			for(int z = 0; z < pesosXZ[0].length; z++){
				data += pesosXZ[x][z]+" ";
			}
		}
		data += "\n";
		for(int z = 0; z < biasZ.length; z++){
			data += "Bias Z: "+biasZ[z]+"\n";
		}

		double error = 0;
		
		ConfusionMatrix.resetVariables();
		

		for (int test = 0; test < validationSet.size(); test++) {
			feedForward(test,validationSet);

			for(int y = 0; y < yNeurons.length; y++){
				error += Math.pow(validationTargets.get(test).get(y) - yNeurons[y],2);

			}

			generateConfusionMatrix(test, validationTargets);
		}

		error = error/(validationSet.size() * yNeurons.length);

		writeData += String.format("%.18f", error)+"\n";
		System.out.println("Error Validation: \t"+String.format("%.18f", error));
		targetSaida += "\tError Validation: "+String.format("%.18f", error)+"\n";
		
		String confusionMatrix = ConfusionMatrix.print();
		DataStateMLP.saveDataStateMLP(pesosXZ, pesosZY, biasZ, biasY, actualEpoch, error, confusionMatrix);


	}

	/*
	**********************************************
	*				 RUN TEST SET				 *
	**********************************************
	*/

	public static String format (double x){
		return String.format("%.4f", x);
	}

	static void test(){

		double error = 0;
		targetSaida += "\n";
		
		for (int test = 0; test < testSet.size(); test++) {
			feedForward(test,testSet);
		
			generateConfusionMatrix(test, testTargets);

			for(int y = 0; y < yNeurons.length; y++){
				error += Math.pow(Math.abs(testTargets.get(test).get(y) - yNeurons[y]),2);
				if(debug){
					System.out.printf("Target: %.4f - Saida: %.4f - ", testTargets.get(test).get(y), yNeurons[y]);	
				}
				
				targetSaida += "Target: "+format(testTargets.get(test).get(y)) +" - Saida: "+format(yNeurons[y])  +"\n"; 
				
			}
			targetSaida+="\n\n";
			
			double diferenca = 0;
			for(int y = 0; y < yNeurons.length; y++){
				diferenca = Math.abs(testTargets.get(test).get(y) - yNeurons[y]);
			}

			String cor = "";
			String reset_cor = "";
			String currentOs = System.getProperty("os.name").toUpperCase();

			
			if(!currentOs.contains("WINDOWS")){
				if(diferenca > 0.07){
				
					cor = "\u001B[31m ";
				}else{
					cor = "\u001B[32m ";
				}
				reset_cor = "\u001B[0m";
			}
			
			if(debug){
				System.out.print("Diferenca: "+ cor);
				System.out.printf("%.2f", diferenca);
				System.out.print(reset_cor+"\n");

				System.out.println();
			}
			
			
		}
		DataManagement.writeData("targetSaida", targetSaida);
		System.out.println("\n*** INFORMATIONS ***");
		System.out.println("Total Tests: "+testSet.size());
		System.out.println("Mean Square Error of Test Set: "+String.format("%.18f",error/(testSet.size() * yNeurons.length))+"\n");
		
		ConfusionMatrix.print();
		
	}

	static public void generateConfusionMatrix(int test, ArrayList<ArrayList<Double>> targets){

		if(problemType.equals("binario")){
			double guess = yNeurons[0];
			double target = targets.get(test).get(0);
			

			if(guess > 0.7){
				guess = 1;
			}else{
				guess = 0;
			}

			ConfusionMatrix.add(target, guess);
			
		}else{
			if (layerY == 1){
				int closestTargetIndex = 0; //pegar índice da diferença menor entre resposta e target - índice referente ao target mais próximo

				for (int i = 0; i < targets.size(); i++) {
					double actualDifference = Math.abs(targets.get(i).get(0) - yNeurons[0]);
					double bestDifference = Math.abs(targets.get(closestTargetIndex).get(0) - yNeurons[0]);
					if(actualDifference < bestDifference){
						closestTargetIndex = i;
					}
				}
				
				int targetRounded = (int)(targets.get(test).get(0)*10); // TARGET ESPERADO
				int guessRounded = (int)(targets.get(closestTargetIndex).get(0)*10); // TARGET PREDITO
			

				if(targetRounded == 10){
					targetRounded = 9;
				}
				if(guessRounded == 10){
					guessRounded = 9;
				}
				
				ConfusionMatrix.add(targetRounded, guessRounded); 
			}else {
				int expectedIndex = 0; 
				int greaterValueIndex = 0;
				for(int y = 0; y < yNeurons.length; y++){
					if(targets.get(test).get(y) == 1.0){
						expectedIndex = y;
					}
					if(yNeurons[greaterValueIndex] < yNeurons[y]){
						greaterValueIndex = y;
					}
				}

				ConfusionMatrix.add(expectedIndex, greaterValueIndex);
			}
		}
	}
	
	public static void updateBestWeight(){
		DataStateMLP best = DataStateMLP.findMinimumError();

		pesosXZ = best.pesosXZ.clone();
		pesosZY = best.pesosZY.clone();
		biasZ = best.biasZ.clone();
		biasY = best.biasY.clone();
		

		String data = "\n";				
		data += "Pesos ZY: ";
		for(int z = 0; z < pesosZY.length; z++){
			for(int y = 0; y < pesosZY[0].length; y++){
				data += pesosZY[z][y]+" ";
			}
			
		}
		data += "\n";
		for(int y = 0; y < biasY.length; y++){
			data += "Bias Y: "+biasY[y]+"\n";
		}

		data += "\n";
		data += "Pesos XZ: ";			
		for(int x = 0; x < pesosXZ.length; x++){
			for(int z = 0; z < pesosXZ[0].length; z++){
				data += pesosXZ[x][z]+" ";
			}
		}
		data += "\n";
		for(int z = 0; z < biasZ.length; z++){
			data += "Bias Z: "+biasZ[z]+"\n";
		}
	}
}