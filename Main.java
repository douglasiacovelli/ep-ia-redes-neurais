import java.io.*;
import java.lang.*;
import java.security.*;
import java.util.*;

public class Main{

	static String parameters;

	public static void main(String[] args) throws FileNotFoundException{
		
		
		if(args.length != 10){
			System.out.println("\n\nUsage: java Main <trainingData> <validationData> <testData> <alpha> <layerZ> <weights> <problemType> <alphaDecreasing> <shuffle> <layerY>");
			System.out.println("\ntrainingData: Nome do arquivo do conjunto de dados de treino");
			System.out.println("validationData: Nome do arquivo do conjunto de dados de validação");
			System.out.println("testData: Nome do arquivo do conjunto de dados de teste");
			System.out.println("alpha: (0 < alpha <= 1) Taxa de aprendizagem");
			System.out.println("layerZ: Numero de neuronios na camada escondida");
			System.out.println("weights: (zero/aleatorio) Inicializao dos pesos");
			System.out.println("problemType: (binario/multiclasse)");
			System.out.println("alphaDecreasing: (true/false) ");
			System.out.println("shuffle: (true/false)");
			System.out.println("layerY: (1/n) <N valido apenas para multiclasse>\n");
			System.out.println("Para mais informacoes leia o README.txt\n\n");
			System.exit(0);
		}
	
		verify(args);
		
		parameters = "java Main ";

		for (int i = 0; i < args.length; i++) {
			parameters += args[i]+" ";
		}
		parameters += "\n";

		//atribuicao dos parametros
		String trainingFile = args[0];
		String validationFile = args[1];
		String testFile = args[2];
		MLP.alpha = Double.parseDouble(args[3]);
		MLP.layerZ = Integer.parseInt(args[4]);
		MLP.inicializeWeights = args[5];
		MLP.problemType = args[6];					
		MLP.alphaDecreasing = Boolean.parseBoolean(args[7]);
		MLP.shuffle = Boolean.parseBoolean(args[8]);

		
		/*******************Training***********************/
		Data training = DataManagement.readData(trainingFile, args[9]);
		MLP.dataSet = PreProcessing.normalize(training.dataSet);
		MLP.targets = training.targets;
		
		/*******************Validation***********************/
		Data validation = DataManagement.readData(validationFile, args[9]);
		MLP.validationSet = PreProcessing.normalize(validation.dataSet);
		MLP.validationTargets = validation.targets;

		/*******************Test***********************/
		Data test = DataManagement.readData(testFile, args[9]); 
		MLP.testSet = PreProcessing.normalize(test.dataSet);
		MLP.testTargets = test.targets;
		

		if(MLP.problemType.equals("multiclasse")){
			MLP.differentTargetsValues = DataManagement.getDifferentTargetsValues();
		}

		if(args[9].equals("n") && MLP.problemType.equals("multiclasse")){
			MLP.layerY = MLP.differentTargetsValues.size(); //Saber o tamanho que cada target terá	
		}else{
			MLP.targets = PreProcessing.normalize(training.targets);
			MLP.validationTargets = PreProcessing.normalize(validation.targets);
			MLP.testTargets = PreProcessing.normalize(test.targets);
			MLP.layerY = 1;
		}

		System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
		System.out.println("Treinamento da Rede Neural Iniciado com arquivo "+ trainingFile);
		System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n");

		MLP.practice();

		System.out.println("\n::::::::::::::::::::::::::::::::::::::::::::::::::::::");
		System.out.println("Testes Iniciados com arquivo "+ testFile);
		System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::\n");

		MLP.test();



		DataStateMLP.writeDataStateMLP();
	}


	/*
	 * Metodo para verificação dos parametros 
	 */

	private static void verify(String[] args) throws FileNotFoundException{
		File f = new File(args[0]);
		if(!f.exists()){
			throw new FileNotFoundException("Invalid Training File");
		}
		f = new File(args[1]);
		if(!f.exists()){
			throw new FileNotFoundException("Invalid Validation File");
		}
		f = new File(args[2]);
		if(!f.exists()){
			throw new FileNotFoundException("Invalid Test File ");
		}
		if(Double.parseDouble(args[3]) <= 0 || Double.parseDouble(args[3]) > 1){
			throw new InvalidParameterException("Alpha must to be '0 < alpha <= 1'");
		}
		try{
			if(Integer.parseInt(args[4]) <= 0) throw new InvalidParameterException("LayerZ need to be positive > 0");
		}catch(Exception e){
			throw new NumberFormatException("LayerZ need to be a number");
		}
		if(!args[5].equals("zero") && !args[5].equals("aleatorio")){
			throw new InvalidParameterException("Weight must to be 'zero' or 'aleatorio'");
		}
		if(!args[6].equals("binario") && !args[6].equals("multiclasse")){
			throw new InvalidParameterException("Problem must to be 'binario' or 'multiclasse'");
		}
		if(!args[7].equals("true") && !args[7].equals("false")){
			throw new InvalidParameterException("AlphaDecreasing must to be 'true' or 'false'");
		}
		if(!args[8].equals("true") && !args[8].equals("false")){
			throw new InvalidParameterException("Shuffle must to be 'true' or 'false'");
		}

		if(!args[9].equals("1") && !args[9].equals("n")){
			throw new InvalidParameterException("LayerY must to be '1' or 'n'");
		}
	}
	
}